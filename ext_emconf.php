<?php

$EM_CONF['twoh_anomaly_detection'] = array(
    'title' => 'TWOH Anomaly Detection',
    'description' => 'An extension that uses AI algorithms to detect unusual activity or patterns on the website that could indicate security risks, fraud, or technical issues.',
    'category' => 'plugin',
    'author' => 'Andreas Reichel',
    'author_email' => 'a.reichel91@outlook.com',
    'author_company' => 'TWOH',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '12.4.99',
            'php' => '8.0-8.3'
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
);
