# TWOH Anomaly Detection

An extension that uses AI algorithms to detect unusual activity or patterns on the website that could indicate security risks, fraud, or technical issues.

## Minimum requirements

* **PHP** 8
* **composer** ^2
* **TYPO3** 12

## Setup

##### Install

* install Extension via Composer or FTP
* include Extension in TypoScript **ROOT Template**

##### Commands
###### Train the AI model
```shell
./vendor/bin/typo3 twoh:train
```

###### Predict anomalies
```shell
./vendor/bin/typo3 twoh:predict
```