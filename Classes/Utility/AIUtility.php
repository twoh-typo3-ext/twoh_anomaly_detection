<?php

declare(strict_types=1);

namespace TWOH\TwohAnomalyDetection\Utility;

class AIUtility
{
    /**
     * @param string $logDirectory
     * @return array
     */
    public static function getDataFromLogFiles(string $logDirectory): array
    {
        $data = [];
        $files = glob($logDirectory . '*.log');

        foreach ($files as $file) {
            $lines = file($file);
            foreach ($lines as $line) {
                $data[] = self::transformLine($line);
            }
        }

        return $data;
    }

    /**
     * @param string $line
     * @return string[]
     */
    public static function transformLine(string $line): array
    {
        return [$line];
    }
}