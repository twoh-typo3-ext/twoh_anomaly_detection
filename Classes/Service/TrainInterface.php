<?php

namespace TWOH\TwohAnomalyDetection\Service;

interface TrainInterface
{
    public function train(): void;
}