<?php

declare(strict_types=1);

namespace TWOH\TwohAnomalyDetection\Service;

use Rubix\ML\AnomalyDetectors\IsolationForest;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\PersistentModel;
use Rubix\ML\Persisters\Filesystem;
use TWOH\TwohAnomalyDetection\Utility\AIUtility;

class TrainService implements TrainInterface
{
    /**
     * @var string $logDirectory
     */
    private string $logDirectory;

    /**
     * @var string $modelDirectory
     */
    private string $modelDirectory;

    /**
     * @var string $modelFile
     */
    private string $modelFile;

    /**
     * @param string $logDirectory
     * @param string $modelDirectory
     * @param string $modelFile
     */
    public function __construct(
        string $logDirectory,
        string $modelDirectory,
        string $modelFile
    )
    {
        $this->logDirectory = $logDirectory;
        $this->modelDirectory = $modelDirectory;
        $this->modelFile = $modelFile;

        $this->train();
    }

    /**
     * @return void
     */
    public function train(): void
    {
        $data = AIUtility::getDataFromLogFiles($this->logDirectory);
        $dataset = new Unlabeled($data);
        $anomalyDetector = new IsolationForest(100, 1);
        $anomalyDetector->train($dataset);

        // Speichern des trainierten Modells
        $persistable = new PersistentModel(
            $anomalyDetector,
            new Filesystem($this->modelDirectory . $this->modelFile)
        );
        $persistable->save();
    }
}