<?php

namespace TWOH\TwohAnomalyDetection\Service;

interface PredictServiceInterface
{
    public function predict(array $item): bool;
}