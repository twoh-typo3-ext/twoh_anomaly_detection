<?php

declare(strict_types=1);

namespace TWOH\TwohAnomalyDetection\Service;

use Rubix\ML\PersistentModel;
use Rubix\ML\Persisters\Filesystem;

class PredictService implements PredictServiceInterface
{
    /**
     * @var string $modelDirectory
     */
    private string $modelDirectory;
    
    /**
     * @var string $modelFile
     */
    private string $modelFile;

    /**
     * @param string $modelDirectory
     * @param string $modelFile
     */
    public function __construct(
        string $modelDirectory,
        string $modelFile
    )
    {
        $this->modelDirectory = $modelDirectory;
        $this->modelFile = $modelFile;
    }

    /**
     * @param array $item
     * @return bool
     */
    public function predict(array $item): bool
    {
        // Laden des gespeicherten Modells
        $persistable = PersistentModel::load(
            new Filesystem(
                $this->modelDirectory . $this->modelFile
            )
        );
        return $persistable
            ->estimator()
            ->predictSample($item);
    }
}