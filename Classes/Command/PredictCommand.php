<?php

declare(strict_types=1);

namespace TWOH\TwohAnomalyDetection\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TWOH\TwohAnomalyDetection\Service\PredictService;
use TWOH\TwohAnomalyDetection\Utility\AIUtility;
use TYPO3\CMS\Core\Core\Environment;

class PredictCommand extends Command
{
    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription('Predict Anomalies in TYPO3 Logs');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $predictService = new PredictService(
            Environment::getVarPath() . '/rubi-xml/',
            'log-ki-model.rbx'
        );

        $logLines = AIUtility::getDataFromLogFiles(
            Environment::getVarPath() . '/log/'
        );

        foreach ($logLines as $logLine) {
            $prediction = $predictService->predict($logLine);

            if (!$prediction) {
                $output->writeln('Anomaly detected for log line: ' . $logLine);

                // @ToDo: if there is a anomaly in log files, sammle diese und schreibe dann eine Mail an den Administrator
            }
        }

        return Command::SUCCESS;
    }
}