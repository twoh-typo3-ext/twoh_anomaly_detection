<?php

declare(strict_types=1);

namespace TWOH\TwohAnomalyDetection\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TWOH\TwohAnomalyDetection\Service\TrainService;
use TYPO3\CMS\Core\Core\Environment;

class TrainCommand extends Command
{
    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription('Train KI-Model for Anomaly Detection');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        new TrainService(
            Environment::getVarPath() . '/log/',
            Environment::getVarPath() . '/rubi-xml/',
            'log-ki-model.rbx'
        );

        return Command::SUCCESS;
    }
}