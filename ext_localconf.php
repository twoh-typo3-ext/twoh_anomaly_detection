<?php

use TWOH\TwohCareer\Controller\JobadController;
use TWOH\TwohCareer\Routing\Aspect\CareerDetailPageMapper;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') || die('Access denied.');
//
//$GLOBALS['TYPO3_CONF_VARS']['SYS']['routing']['aspects']['CareerDetailPageMapper']
//    = CareerDetailPageMapper::class;

ExtensionUtility::configurePlugin(
    'TwohCareer',
    'CareerDetails',
    // all actions
    [
        JobadController::class => 'show'
    ],
    // non-cacheable actions
    [
        JobadController::class => ''
    ]
);

ExtensionUtility::configurePlugin(
    'TwohCareer',
    'CareerFilter',
    // all actions
    [
        JobadController::class => 'filter'
    ],
    // non-cacheable actions
    [
        JobadController::class => 'filter'
    ]
);

ExtensionUtility::configurePlugin(
    'TwohCareer',
    'CareerList',
    // all actions
    [
        JobadController::class => 'list'
    ],
    // non-cacheable actions
    [
        JobadController::class => 'list'
    ]
);